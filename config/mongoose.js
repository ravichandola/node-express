// require the library 
const mongoose = require('mongoose');

// connect to the database
mongoose.connect('mongodb://localhost/contact_list_db');

//acquire the connection (to check if it is succesful)
const db = mongoose.connection;

db.on('error', console.error.bind('Error connecting to DB'));

// up and running then print the message
db.once('open',function(){
    console.log('Successfully connected to database');
});
